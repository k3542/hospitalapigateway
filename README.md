# Hospital API Gateway

[![pipeline status][pipeline-badge]][commits-gl]

## Daftar Isi

- [Daftar Isi](#daftar-isi)
- [Informasi Kelompok](#informasi-kelompok)
- [Deskripsi](#deskripsi)

## Informasi Kelompok

Penanggung Jawab Hospital API Gateway: Rico Tadjudin

Kelompok 8 LAW-B:
- Alisha Yumna Bakri - 1906400173
- Muhammad Fathan Muthahhari - 1906293190
- Muzaki Azami - 1806205470
- Rico Tadjudin - 1906398364
- Samuel Mulatua Jeremy - 1906308305

## Deskripsi

Hospital API Gateway adalah gerbang utama API bagi Hospital Service Kelompok 8 LAW-B. Pada instance ini, ditempatkan sebuah NGINX server yang akan berperan sebagai reverse proxy. Di antara lain, setiap request atau pemanggilan ke gateway ini akan diproses (bila perlu) dan diteruskan ke tempat/service yang seharusnya.

API Gateway ini juga akan melakukan caching dan load balancing untuk pemanggilan ke service search medicine atau servis pencarian obat. Hal ini karena diperkirakan servis pencarian ini akan memiliki traffic yang banyak.


[commits-gh]: https://github.com/k3542/hospitalapigateway/commits/master
[pipeline-badge]: https://gitlab.com/k3542/hospitalapigateway/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/k3542/hospitalapigateway/-/commits/master